<?php

// src/Command/CreateUserCommand.php
namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ScrapingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:scraping')

            // the short description shown while running "php bin/console list"
            ->setDescription('Scraping emails...');

            // the full command description shown when running the command with
            // the "--help" option
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Definimos la función cURL
        function curl($url) {
            $ch = curl_init($url); // Inicia sesión cURL
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // Configura cURL para devolver el resultado como cadena
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Configura cURL para que no verifique el peer del certificado dado que nuestra URL utiliza el protocolo HTTPS

            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            $info = curl_exec($ch); // Establece una sesión cURL y asigna la información a la variable $info
            curl_close($ch); // Cierra sesión cURL
            return $info; // Devuelve la información de la función
        }

        $progress = new ProgressBar($output);
        $progress->start();

        $i = 1;
        $emailsList = array();
        while ($i <= 400) {
            $sitioweb = curl("https://www.paginasamarillas.com.ar/b/farmacias/p-".$i."/");  // Ejecuta la función curl escrapeando el sitio web https://devcode.la and regresa el valor a la variable $sitioweb
            preg_match_all('#\bhttp?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $sitioweb, $match);
            $duplicateRemovedArraySitioWeb = (array_unique($match[0]));

            foreach ($duplicateRemovedArraySitioWeb as $link){
                $string_search = curl($link); // get de url
//                if ((strpos($string_search, 'wordpress')) == false){
                    $pattern	=	"/(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/";
//                    var_dump(preg_match_all($pattern, $string_search, $matches));
                    if (preg_match_all($pattern, $string_search, $matches) != 0){
                        preg_match_all($pattern, $string_search, $matches);
                        $duplicateRemovedArrayEmail = (array_unique($matches[0])); //limpia los emails

                        foreach($duplicateRemovedArrayEmail as $email){
//                            var_dump($email);
                            array_push($emailsList, $email);
                        }
                    }

//                    $patternTitle = '/[<]title[>]([^<]*)[<][\/]titl/i';
//                    if ( preg_match($patternTitle, $string_search, $matchesTitle) != 0){
//                        preg_match($patternTitle, $string_search, $matchesTitle);
//                        $duplicateRemovedArrayTitle = (array_unique($matchesTitle[0]));
//                    }


//                }
            }
            $i++;
            $progress->advance();

        }
        $duplicateRemoved = (array_unique($emailsList)); //limpia los emails

        $fichier = $this->getContainer()->get('kernel')->getRootDir() . '/../public/files/emails.csv';

//        header( "Content-Type: text/csv;charset=utf-8" );
//        header( "Content-Disposition: attachment;filename=\"$fichier\"" );
//        header("Pragma: no-cache");
//        header("Expires: 0");

        $fp= fopen($fichier, 'w');

        fputcsv($fp, $duplicateRemoved);
        $progress->finish();
    }

}
